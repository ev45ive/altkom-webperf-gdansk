git clone https://bitbucket.org/ev45ive/altkom-webperf-gdansk.git
npm install
npm start

# New package 
npm init -y 

# Development simple server
npm install http-server --save-dev 

# Normalize differences between browsers
https://necolas.github.io/normalize.css/

# Faking server response with node
npm install express --save

node ./server/index.js

# Node monitor
npm i -g nodemon

nodemon ./server/index.js

nodemon.cmd .\server\index.js 

# Expires, Cache-Control, ETag, long-cache + link name

# Gzip compression
npm install compression -D

# Limit DNS and Keep Alive
https://zaufanatrzeciastrona.pl/wp-content/uploads/2014/06/tcp-joke.png

# Style, Layout, Paint, Compose
http://jankfree.org/

# OOCSS
http://www.stubbornella.org/content/2010/06/25/the-media-object-saves-hundreds-of-lines-of-code/

https://www.keycdn.com/blog/oocss#oocss-vs-smacss-vs-bem

http://styleguides.io/

https://airbnb.design/building-a-visual-language/

https://www.lightningdesignsystem.com/

https://www.carbondesignsystem.com/

# SCSS 

https://sass-lang.com/documentation/style-rules/placeholder-selectors

https://css-tricks.com/snippets/sass/bem-mixins/

https://github.com/uncss/uncss

# CSS modules

https://github.com/css-modules/css-modules

https://www.postcss.parts/tag/sass

https://blog.bitsrc.io/how-to-use-sass-and-css-modules-with-create-react-app-83fa8b805e5e

# Concate images

- https://www.w3schools.com/html/html_images_imagemap.asp

- Sprites

- SVG

- Font Icons

- Inline base64 

```css
.backgroundA {
    background-image: url(" data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAIAAACRXR/
    ...
    CntJ2lkmytznwZFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYW1qsrwABYuwNkimqm3gAAAABJRU5ErkJggg==");
}
```
```css
w-do-not{
    /* visibility: hidden; */
    display: none;
}

.workplace-img{
    background: url('/styles/workplace.jpg') no-repeat;
}
```

```js
// preload
img = new Image()
img.onload = console.log ;
img.src = 'http://localhost:8080/styles/workplace.jpg'
```

# client hints

https://developer.mozilla.org/en-US/docs/Glossary/Client_hints

https://developer.mozilla.org/en-US/docs/Learn/HTML/Multimedia_and_embedding/Responsive_images

# Layout thrashing
https://gist.github.com/paulirish/5d52fb081b3570c81e3a

https://developers.google.com/web/fundamentals/performance/rendering/avoid-large-complex-layouts-and-layout-thrashing?hl=en

https://github.com/wilsonpage/fastdom

# Layer promotion
https://developer.mozilla.org/en-US/docs/Web/CSS/will-change

# Webpack
npm install webpack webpack-cli --save-dev
npm i -g webpack-cli
npm i -D webpack-dev-server 

npm install --save-dev html-webpack-plugin
npm install --save-dev clean-webpack-plugin

npm install -D style-loader css-loader sass-loader

<!-- npm install -D extract-text-webpack-plugin -->

npm install -D mini-css-extract-plugin

https://survivejs.com/webpack/loading/images/

# JS compiler
= JIT 
= Speculative / heuristic optimizing compiler - TurboFan

https://ponyfoo.com/articles/an-introduction-to-speculative-optimization-in-v8

https://alligator.io/js/v8-engine/

https://mathiasbynens.be/notes/shapes-ics

https://blog.logrocket.com/how-javascript-works-optimizing-the-v8-compiler-for-efficiency/

https://www.youtube.com/watch?v=cvybnv79Sek


# Microtasks macro tasks - event loop + scheduling (queue)
https://jakearchibald.com/2015/tasks-microtasks-queues-and-schedules/

https://www.youtube.com/watch?v=cCOL7MC4Pl0

https://medium.com/@Rahulx1/understanding-event-loop-call-stack-event-job-queue-in-javascript-63dcd2c71ecd

# WebWorkers
 https://github.com/webpack-contrib/worker-loader

# Service Workers

https://serviceworke.rs/

https://developers.google.com/web/tools/workbox

https://developers.google.com/web/tools/workbox/modules/workbox-webpack-plugin
https://developers.google.com/web/tools/workbox/modules/workbox-cli

https://developers.google.com/web/tools/workbox/modules/workbox-strategies
https://developers.google.com/web/tools/workbox/modules/workbox-cache-expiration


# Lighthouse

- First paint / meaningful
https://developers.google.com/web/fundamentals/architecture/app-shell

# Lazy load images
https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API

https://github.com/dinbror/blazy#demo

https://css-tricks.com/snippets/html/base64-encode-of-1x1px-transparent-gif/


# Http2 / multiplex / push
https://www.phproundtable.com/episode/all-about-http2

