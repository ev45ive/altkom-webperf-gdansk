var path = require('path')
var HtmlWebpackPlugin = require('html-webpack-plugin');
var { CleanWebpackPlugin } = require('clean-webpack-plugin');
var MiniCssExtractPlugin = require('mini-css-extract-plugin')
const {GenerateSW} = require('workbox-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {

    // devtool:'sourcemap',

    mode: 'development',

    entry: {
        main: path.join(__dirname, 'src', 'index.js'),
        // https://github.com/webpack-contrib/worker-loader
        worker: path.join(__dirname, 'src', 'worker.js'),    
    },

    output: {
        path: path.join(__dirname, 'dist'),
        filename: '[name].js'
    },

    module: {
        rules: [
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader']
            },
            {
                test: /\.scss$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
            },
            {
                test: /\.(png|jpe?g|gif)$/i,
                loader: 'url-loader',
                options: {
                    name: './images/[name].[hash].[ext]',
                    limit: 8192, // in bytes,
                    fallback: 'file-loader'
                },
            }
        ]
    },

    plugins: [
        new CleanWebpackPlugin(),
        new CopyPlugin([
            { from: 'src/resources0/*.jpg', to: 'images' },
            { from: 'src/resources0/*.png', to: 'images' },
        ]),
        new MiniCssExtractPlugin({
            filename: '[name].[contenthash].css',
            chunkFilename: '[name].[contenthash].css',
            
        }),
        new HtmlWebpackPlugin({
            template: 'src/index.html',
            chunks:['main'],
            minify:true,
        }),
        new GenerateSW()
    ],

    devServer: {
        // port: 8080,
        // writeToDisk: true
    }
}