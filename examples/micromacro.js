console.log('test 1')

requestAnimationFrame(()=>{
    console.log('test requestAnimationFrame')
})

new Promise((resolve)=>{

    Promise.resolve('test promise 2')
    .then(console.log)

    resolve('test promise 1')
})
.then(console.log)

setTimeout(()=>{
    console.log('test timeout')
},0)

console.log('test 2')