var express = require('express')
var compression = require('compression')

var app = express()
app.use(compression({
    threshold: 0
}))

// app.set('etag',false)

app.get('/', (req, res) => {
    console.log('request')

    res.send(`Test
        <script src="zasob/?placki=hash_zasobu9"></script>
    `)
})

app.get('/zasob', (req, res) => {
    // res.removeHeader('Connection')
    res.setHeader('Expires', (new Date(2021, 0, 8, 11, 41)).toUTCString())
    // res.setHeader('Cache-Control','public, max-age=1600')
    // res.setHeader('ETag','test123')
    res.setHeader('Content-Type', 'text/javascript; charset=UTF-8')
    res.send(`console.log('Zasob!');`)
})


app.listen(3000, () => {
    console.log("Server running...")
})