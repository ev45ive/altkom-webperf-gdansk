module.exports = {
  "globDirectory": "src/",
  "globPatterns": [
    "**/*.{js,html,gif,css,jpg}"
  ],
  "swDest": "dist/sw.js"
};